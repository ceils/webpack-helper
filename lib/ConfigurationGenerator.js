const defaultConfiguration = require('./provider/DefaultConfiguration');

class ConfigurationGenerator {

    constructor() {
        this.config = defaultConfiguration;
    }

}

module.exports = ConfigurationGenerator;