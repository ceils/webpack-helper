class RuntimeConfig {
    constructor() {
        this.command            = [];
        this.helpRequested      = false;
        this.isValidCommand     = false;
        this.environnement      = '';
        this.context            = '';
        this.babelRcFileExists  = false;
    }
}

module.exports = RuntimeConfig;