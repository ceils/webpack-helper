const RuntimeConfig = require('./RuntimeConfig');
const pkgUp         = require('pkg-up');
const path          = require('path');
const resolveRc     = require('babel-loader/lib/resolve-rc');

class RuntimeParser {
    constructor(runtimeConfig) {
        this.runtimeConfig = runtimeConfig;
    }

    parse(argv, cwd) {
        this.runtimeConfig.command = argv._[0];

        switch(this.runtimeConfig.command) {
            case 'dev':
                this.runtimeConfig.isValidCommand = true;
                this.runtimeConfig.environnement  = 'dev';
                break;
            case 'dev-server':
                this.runtimeConfig.isValidCommand = true;
                this.runtimeConfig.environnement  = 'dev';
                break;
            case 'production':
                this.runtimeConfig.isValidCommand = true;
                this.runtimeConfig.environnement  = 'production';
                break;                        
        }

        this.runtimeConfig.context = argv.context;
        if (void 0 === this.runtimeConfig.context) {
            const packagePath = pkgUp.sync(cwd);

            if (null === packagePath) {
                throw new Error('Cannot determine webpack context. (Are you executing webpack from a directory outside of your project?). Try passing the --context option.');
            }

            this.runtimeConfig.context = path.dirname(packagePath);
        }

        if (argv.h || argv.help) {
            this.runtimeConfig.helpRequested = true;
        }

        this.runtimeConfig.babelRcFileExists = void 0 !== (resolveRc(require('fs'), this.runtimeConfig.context));

        return this.runtimeConfig;
    }
}

module.exports = RuntimeParser;