const path          = require('path');
const runtimeConfig = require('./lib/context').runtimeConfig;

const config = {
    context: path.join(__dirname, 'app'),
    entry: {
        'app': path.join(__dirname, 'app/app.js') 
    },
    output: {
        path: path.join(__dirname, 'app/build'),
        filename: '[name].js'
    },
    stats: {
        error: false
    }
};

console.log(config);


module.exports = config;